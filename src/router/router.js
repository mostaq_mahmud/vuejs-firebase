import Index from '../components/Index.vue'
import AddSmoothie from '../components/AddSmoothie'
import EditSmoothie from '../components/EditSmoothie'
import SlotPage from '../components/SlotPage'
export const routes = [ 
    {path: '/', component: Index, name: 'Index'},
    {path: '/add-smoothie', name: 'AddSmoothie', component: AddSmoothie},
    {path: '/edit-smoothie/:smoothie_slug', name: 'EditSmoothie', component: EditSmoothie},
    {path: '/test-page', component: SlotPage}

];