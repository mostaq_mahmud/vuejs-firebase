import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import {routes} from "./router/router";


/*Use VueRouter*/
Vue.use(VueRouter);
Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history', //hides hash in url after localhost:8080
  routes: routes
});

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
